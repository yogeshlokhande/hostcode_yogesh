package dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import controller.Application;
import java.util.Date;
import java.util.List;

@Repository
public class Dao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public String findProjectId(String dataSourceId){
    	String sql = "select ownerProjectId from  datasourceinstance where dataSourceid=?";
    	String projectId = (String) jdbcTemplate.queryForObject(sql, new Object[] { dataSourceId }, String.class);
    	return projectId;
    }
    public Dao() {
    	jdbcTemplate = (JdbcTemplate) Application.context.getBean("jdbcTemplate");
    }
}