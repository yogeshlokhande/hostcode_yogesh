package utility;

public class Packet {
	
	private byte[] bytes;
	private String filePath;
	String readyId;

    public Packet (byte[] bytes, String filePath, String readyId) {
    	this.bytes = bytes;
    	this.filePath = filePath;
    	this.readyId = readyId;
    }
    
    public byte[] getBytes() {
    	return bytes;
    }  
    
    public String getFilePath() {
    	return filePath;
    }
    
    public String getReadyId() {
    	return readyId;
    }
}