package utility;


public class LoggerConstants {
	// error logs
	
	public static final String LOG_MAXIQWEB = "MaxIQWeb ";
	public static final String LOG_ETL_DESIGN = "ETL_DESIGN";
	public static final String LOG_ETL_EXECUTION = "ETL_EXECUTION";
	public static final String LOG_APP_DESIGN = "APP_DESIGN";
	public static final String LOG_APP_EXECUTION = "APP_EXECUTION";
	public static final String LOG_MESSAGES = "MESSAGES";
	public static final String LOG_AGENT = "AGENT";
	public static final String LOG_DSUTILITY = "DS_UTILITY_EXECUTOR";
	public static final String LOG_APP_MACHINE_LEARNING = "APP_MACHINE_LEARNING";

}
