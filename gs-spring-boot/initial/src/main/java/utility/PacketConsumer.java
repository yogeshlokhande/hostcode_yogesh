package utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;

public class PacketConsumer implements Runnable {

	private final BlockingQueue<Packet> sharedQueue;
	private static final Logger logger = LoggerFactory.getLogger(PacketConsumer.class);
	public static Map<String, Long> wittenPacketCountMap = new HashMap<>();

    public PacketConsumer (BlockingQueue<Packet> sharedQueue) {
        this.sharedQueue = sharedQueue;
    }
  
    @Override
    public void run() {
        while(true){
            try {
                Packet packet = sharedQueue.take();
                FileWriter fileWriter = new FileWriter();
                boolean isWriiten = fileWriter.writeFile(packet.getBytes(), packet.getFilePath());
                if (isWriiten) {
                	String readyId = packet.getReadyId();
                	if (null == wittenPacketCountMap.get(readyId) ) {
                		wittenPacketCountMap.put(readyId, 1L);
                	} else {
                		Long packetCount = (Long) wittenPacketCountMap.get(readyId);
                		wittenPacketCountMap.put(readyId, ++packetCount);                		
                	}
                }               
            } catch (InterruptedException e) {
            	e.printStackTrace();
            	logger.info("PacketConsumer::run::" + e);
            }
        }
    }
  }