package utility;

public class CharCONSTANTS {
	public static final String SPACE = " ";
	public static final String COMMAND = ",";
	public static final String NO_SPACE = "";
	public static final String PIPE = "|";
	public static final String QUOTE = "\"";
	public static final String SIGNLE_QUOTE = "\'";
	public static final String SEMI_COLON = ";";
	public static final String HYPHEN = "-"; 
	public static final String NEW_LINE = "\n";
	public static final String UNDER_SCORE = "_";
	public static final String COLON = ":";
	public static final String FORWARD_SLASH ="/";
	
}
