package utility;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.tomcat.util.http.fileupload.FileUtils;

public class FileWriter {
	
	public boolean writeFile(byte[] bytes, String filePath) {
				
		boolean isWritten = false;
		try (FileOutputStream fos = new FileOutputStream(filePath, true)) {
			fos.write(bytes);
			isWritten = true;
		} catch(IOException e) {
        	e.printStackTrace();        	
        }	
		return isWritten;
	}
	
	public void createFolder(String filePath) {
		Path path = Paths.get(filePath);
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
             } catch (IOException e) {
                e.printStackTrace();
            }             
        }
	}

	public long getFileCountInFolder(String filePath) {
		System.out.println("                         --------filePath--->>" + filePath);
		return new File(filePath).list().length;
	}
	
}