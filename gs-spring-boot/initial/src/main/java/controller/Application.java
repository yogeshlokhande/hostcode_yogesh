package controller;

import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import utility.PacketConsumer;

@SpringBootApplication
public class Application {
	
	private static final Logger logger = LoggerFactory.getLogger(Application.class);
	
    public static BlockingQueue sharedQueue = new LinkedBlockingQueue();
    
    public static void main(String[] args) {
    //	PropertyConfigurator.configure("log4j.properties");
        ApplicationContext ctx = SpringApplication.run(Application.class, args);
        context = ctx;
        
        logger.info("Application::main:: Let's inspect the beans provided by Spring Boot:");
        
        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
        	logger.info(beanName);
        }
        
        Thread packetsConsumerThread = new Thread(new PacketConsumer(sharedQueue));
        packetsConsumerThread.start();
    }
	public static ApplicationContext context = null;
}
